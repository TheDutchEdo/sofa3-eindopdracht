﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DevOps.Domain;

namespace DevOps
{
    class Program
    {
        [ExcludeFromCodeCoverage]
        static void Main(string[] args)
        {
            Notification notificationService = new Notification();

            Scrummaster scrummaster = new Scrummaster("Edo van Voorst");
            notificationService.AddScrummaster(new UserNotification(scrummaster));
            notificationService.AddUser(new UserNotification(scrummaster));
            ProductOwner productOwner = new ProductOwner("Edo van Voorst");
            notificationService.AddUser(new UserNotification(productOwner));
            LeadDeveloper leadDeveloper = new LeadDeveloper("Edo van Voorst");
            notificationService.AddUser(new UserNotification(leadDeveloper));
            Developer developer = new Developer("Edo van Voorst");
            notificationService.AddUser(new UserNotification(developer));
            Tester tester = new Tester("Edo van Voorst");
            notificationService.AddUser(new UserNotification(tester));
            notificationService.AddTester(new UserNotification(tester));

            Backlog backlog = new Backlog();
            Backlog sprint1Backlog = new Backlog();
            ISprint sprint1 = new DeploymentSprint(sprint1Backlog, 1, "Testsprint");
            sprint1.UpdateStatus(new Unfinished());

            Project project = new Project(
                "First project",
                new List<User> { scrummaster, productOwner, leadDeveloper, developer, tester },
                new List<ISprint> { sprint1 }
            );

            BacklogItem backlogItem = scrummaster.CreateBackLogItem(backlog, "First backlog item");
            Activity activity = scrummaster.CreateActivity("Maak programma", backlogItem);

            scrummaster.AddDeveloperToBackLogItem(developer, backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDoing());

            developer.ChangeActivityStatus(activity, true);

            developer.ChangeFaseBacklogItem(backlogItem, new StateReadyForTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateToDo());

            developer.ChangeFaseBacklogItem(backlogItem, new StateDoing());

            developer.ChangeFaseBacklogItem(backlogItem, new StateReadyForTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateTested());

            leadDeveloper.ChangeFaseBacklogItem(backlogItem, new StateReadyForTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateTesting());

            tester.ChangeFaseBacklogItem(backlogItem, new StateTested());

            //leadDeveloper.ChangeFaseBacklogItem(backlogItem, new StateDone());

            developer.CreateThread("Code coverage", backlogItem);

            Thread thread = backlogItem.GetThread("Code coverage");
            developer.SendMessageToThread("De code coverage moet 80% zijn", thread);
            leadDeveloper.SendMessageToThread("Ik vind dat hij 90% moet zijn", thread);
            scrummaster.CreateThread("Wat wil de klant?", backlogItem);
            Thread newthread = backlogItem.GetThread("Wat wil de klant?");
            developer.SendMessageToThread("Ik dadcht dat de klant hier geen idee over had", newthread);
            leadDeveloper.SendMessageToThread("Dat klopt doordat hij er geen verstand van heeft.", newthread);
            thread.Add(newthread);
            scrummaster.SendMessageToThread("De po heeft bepaald dat het 85 % moet zijn", thread);

            thread.Display(1);

            sprint1.GeneratePNGRapport();

            scrummaster.RunPipeline(sprint1);
        }
    }
}
