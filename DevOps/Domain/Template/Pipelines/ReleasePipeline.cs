﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class ReleasePipeline : PipelineTemplate
    {
        protected override bool Done()
        {
            return true;
        }

        protected override void Production()
        {
            Console.WriteLine("Deploying to heroku/Production");
        }

        protected override void Staging()
        {
            Console.WriteLine("Deploying to Production stage");
        }

        protected override void Testing()
        {
            Console.WriteLine("Tested Unit-Tested, End to End Tested");
        }
    }
}
