﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class DevelopmentPipeline : PipelineTemplate
    {
        protected override bool Done()
        {
            return true;
        }

        protected override void Production()
        {
            Console.WriteLine("Deploying to heroku/development");
        }

        protected override void Staging()
        {
            Console.WriteLine("Deploying to Development stage");
        }

        protected override void Testing()
        {
            Console.WriteLine("Tested Unit-Tested");
        }
    }
}
