﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class PipeLineRunner
    {
        public static bool RunPipeline(PipelineTemplate pipelineTemplate)
        {
            return pipelineTemplate.Template();
        }
    }
}
