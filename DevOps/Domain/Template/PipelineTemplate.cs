﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public abstract class PipelineTemplate
    {
        public bool Template()
        {
            this.BuildInitiation();
            this.Building();
            this.TestInitiation();
            this.Testing();
            this.StagingInitiation();
            this.Staging();
            this.ProductionInitiation();
            this.Production();
            return this.Done();
        }

        protected void BuildInitiation()
        {
            Console.WriteLine("Starting the building Procedure");
        }

        protected void Building()
        {
            Console.WriteLine("Building the project");
        }

        protected void TestInitiation()
        {
            Console.WriteLine("Starting the Testing Procedure");
        }

        protected void StagingInitiation()
        {
            Console.WriteLine("Starting the Staging Procedure");
        }

        protected void ProductionInitiation()
        {
            Console.WriteLine("Starting the Production Procedure");
        }

        protected abstract void Testing();

        protected abstract void Staging();

        protected abstract void Production();

        protected abstract bool Done();
    }
}
