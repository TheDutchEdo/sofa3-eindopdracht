﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DevOps.Domain
{
    public class BacklogItem
    {
        public string name { get; private set; }
        private Fases currentFase;
        private DateTime createdOn;
        private List<Activity> activities;
        private List<Developer> users;
        private List<Forum> threads;
        public bool isLocked { get; private set; }

        public BacklogItem(string name, DateTime createdOn)
        {
            this.name = name;
            this.currentFase = new StateToDo();
            this.createdOn = createdOn;
            this.activities = new List<Activity>();
            this.users = new List<Developer>();
            this.threads = new List<Forum>();
            this.isLocked = false;
        }

        public void Changename(string name)
        {
            if (!isLocked)
            {
                this.name = name;
            }
        }

        public List<Activity> GetActivities()
        {
            return activities;
        }

        public void AddActivity(Activity activity)
        {
            if (!isLocked)
            {
                activities.Add(activity);
            } 
        }

        public void AddDeveloper(Developer user)
        {
            if (activities.Count > users.Count)
            {
                users.Add(user);
            }
        }

        public List<Developer> GetDevelopers()
        {
            return users;
        }

        public void ChangeFase(Fases fase)
        {
            switch (currentFase.CurrentFase())
            {
                case "Done":
                    foreach (var activity in activities)
                    {
                        if (!activity.GetStatus())
                        {
                            fase = currentFase;
                            Console.WriteLine("Fase cannot be done, because some activities are not done!");
                            Notification.instance.NotifyAllScrummasters();
                        }
                    }
                    break;
                case "ReadyForTesting":
                    Notification.instance.NotifyAllTesters();
                    break;
            }
            currentFase = fase;
        }

        public Fases GetFase()
        {
            return currentFase;
        }

        public Thread GetThread(string message)
        {
            return (Thread)threads.Where(p => p.name == message).FirstOrDefault();
        }

        public void AddThread(string message)
        {
            threads.Add(new Thread(message, this));
        }

        public void AddMessages(string message)
        {
            threads.Add(new Message(message));
        }
        public void LockBacklogItem()
        {
            isLocked = true;
        }
    }
}
