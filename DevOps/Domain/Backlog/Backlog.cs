﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Backlog
    {
        private List<BacklogItem> backlogItems;

        public Backlog()
        {
            backlogItems = new List<BacklogItem>();
        }

        public void AddBackLogItem(BacklogItem backlogItem)
        {
            backlogItems.Add(backlogItem);
        }

        public List<BacklogItem> GetBackLogItems()
        {
            return backlogItems;
        }

        public void LockBackLogItem()
        {
            foreach (var item in backlogItems)
            {
                item.LockBacklogItem();
            }
        }
    }
}
