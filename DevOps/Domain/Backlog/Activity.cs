﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Activity
    {
        public string name { get; private set; }
        private bool status;
        public Activity(string name, bool status)
        {
            this.name = name;
            this.status = status;
        }

        public void ChangeStatus(bool status)
        {
            this.status = status;
        }

        public bool GetStatus()
        {
            return status;
        }
    }
}
