﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevOps.Domain
{
    public class Project
    {
        private string name;
        private List<User> users;
        private List<ISprint> sprints;

        public Project(string name, List<User> users, List<ISprint> sprints)
        {
            this.name = name;
            this.users = users;
            this.sprints = sprints;
        }

        public void AddUser(User user)
        {
            users.Add(user);
        }

        public List<User> GetUsers()
        {
            return users;
        }

        public void AddSprint(ISprint sprint)
        {
            sprints.Add(sprint);
        }

        public List<ISprint> GetSprints()
        {
            return sprints;
        }
    }
}
