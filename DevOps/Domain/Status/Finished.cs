﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Finished : IStatus
    {
        public string GetStatus()
        {
            return STATUS.Finished.ToString();
        }
    }
}
