﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public interface IStatus
    {
        public string GetStatus();
    }
}
