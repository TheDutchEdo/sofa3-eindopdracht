﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Unfinished : IStatus
    {
        public string GetStatus()
        {
            return STATUS.Unfinished.ToString();
        }
    }
}
