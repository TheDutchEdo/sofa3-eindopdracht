﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevOps.Domain
{
    public class PlannedIn : IStatus
    {
        public string GetStatus()
        {
            return STATUS.PlannedIn.ToString();
        }
    }
}
