﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public interface ISprint
    {
        public void AddRapport(Rapport rapport);
        public Rapport GetRapport();
        public Backlog GetBacklog();
        public int GetWeeknumber();
        public string GetStatus();
        public void SetName(string name);
        public string GetName();
        public bool RunPipeline();
        public void UpdateStatus(IStatus status);
        public void AddDeveloper(Developer developer);
        public void AddTester(Tester tester);
        public void ChangeScrummaster(Scrummaster scrummaster);
        public void GeneratePdFRapport();
        public void GeneratePNGRapport();
        public void UpdateWeeknumber(int number);
        public bool CheckSprintStatus();
    }
}
