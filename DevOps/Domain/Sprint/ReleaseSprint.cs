﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class ReleaseSprint : ISprint
    {
        private Backlog sprintbacklog;
        private int weeknumber;
        private string name;
        private IStatus status;
        private Rapport rapport;
        private PipelineTemplate pipeline;
        private List<Developer> developers;
        private List<Tester> testers;
        private Scrummaster scrummaster;

        public ReleaseSprint(Backlog backlog, int weeknumber, string name)
        {
            this.sprintbacklog = backlog;
            this.weeknumber = weeknumber;
            this.name = name;
            status = new PlannedIn();
            pipeline = new ReleasePipeline();
            this.developers = new List<Developer>();
            this.testers = new List<Tester>();
        }

        public void AddDeveloper(Developer developer)
        {
            developers.Add(developer);
        }

        public void AddTester(Tester tester)
        {
            testers.Add(tester);
        }

        public void ChangeScrummaster(Scrummaster scrummaster)
        {
            this.scrummaster = scrummaster;
        }

        public void AddRapport(Rapport rapport)
        {
            if (!status.GetStatus().Equals(STATUS.Finished.ToString()))
                this.rapport = rapport;
        }

        public bool CheckSprintStatus()
        {
            DateTime day = DateTime.Now;
            int weeknumber = day.DayOfYear / 7;

            if(weeknumber > this.weeknumber || status.GetStatus() == STATUS.Finished.ToString()) {
                status = new Finished();
                return true;
            }

            return false;      
        }

        public Backlog GetBacklog()
        {
            return sprintbacklog;
        }

        public Rapport GetRapport()
        {
            return rapport;
        }

        public string GetStatus()
        {
            return status.GetStatus();
        }

        public int GetWeeknumber()
        {
            return weeknumber;
        }

        public bool RunPipeline()
        {
            if (!status.GetStatus().Equals(STATUS.Finished.ToString()))
                if(!PipeLineRunner.RunPipeline(pipeline)) {
                    Notification.instance.NotifyAllScrummasters();
                    Notification.instance.NotifyAllProductowner();
                } else {
                    return true;
                }

            return false;
        }

        public void UpdateStatus(IStatus status)
        {
            this.status = status;

            if (this.status.GetStatus().Equals(STATUS.Finished.ToString()))
            {
                sprintbacklog.LockBackLogItem();
            }
        }
        public void GeneratePdFRapport()
        {
            Rapport rapport = new Rapport(new ExportToPdf(), this);
            AddRapport(rapport);
            rapport.Export();
        }
        public void GeneratePNGRapport()
        {
            Rapport rapport = new Rapport(new ExportToPng(), this);
            AddRapport(rapport);
            rapport.Export();
        }

        public void UpdateWeeknumber(int number)
        {
            if (!status.GetStatus().Equals(STATUS.Finished.ToString()))
                this.weeknumber = number;
        }

        public void SetName(string name)
        {
            if(!status.GetStatus().Equals(STATUS.Finished.ToString()))
                this.name = name;
        }

        public string GetName()
        {
            return name;
        }
    }
}
