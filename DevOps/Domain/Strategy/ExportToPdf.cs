﻿using System;
using System.Collections.Generic;
using System.Text;
using IronPdf;

namespace DevOps.Domain
{
    public class ExportToPdf : ExportStrategy
    {
        public void Export(ISprint sprint)
        {
            var Renderer = new ChromePdfRenderer();
            using var PDF = Renderer.RenderHtmlAsPdf("<h1>Hello World<h1>");
            PDF.SaveAs("export-" + sprint.GetWeeknumber() + ".pdf");
        }
    }
}
