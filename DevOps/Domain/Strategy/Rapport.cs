﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Rapport
    {
        private ExportStrategy strategy;
        private ISprint sprint;

        public Rapport(ExportStrategy strategy, ISprint sprint)
        {
            this.strategy = strategy;
            this.sprint = sprint;
        }

        public void SetStrategy(ExportStrategy strategy)
        {
            this.strategy = strategy;
        }

        public ExportStrategy GetStrategy()
        {
            return this.strategy;
        }

        public void Export()
        {
            this.strategy.Export(sprint);
        }
    }
}
