﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace DevOps.Domain
{
    public class ExportToPng : ExportStrategy
    {
        public void Export(ISprint sprint)
        {
            FileInfo fi = new FileInfo("export-"+ sprint.GetWeeknumber() + ".png");
            FileStream fstr = fi.Create();
            Bitmap bmp = new Bitmap(50, 50);
            bmp.Save(fstr, ImageFormat.Png);
            Console.WriteLine("saved at:" + fstr.Name);
            fstr.Close();
        }
    }
}
