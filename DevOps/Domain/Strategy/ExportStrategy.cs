﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public interface ExportStrategy
    {
        void Export(ISprint sprint);
    }
}
