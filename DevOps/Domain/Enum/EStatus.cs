﻿public enum STATUS
{
    PlannedIn,
    Finished,
    Unfinished
}