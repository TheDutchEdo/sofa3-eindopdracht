﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevOps.Domain
{
    public class StateDone : Fases
    {
        private string faseName = "Done";
        public string CurrentFase()
        {
            return faseName;
        }
    }
}
