﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class LocalStorage
    {
        public Backlog backlog;
        public ISprint sprintRelease;
        public List<ISprint> sprintList;

        public LocalStorage()
        {
            backlog = CreateBacklog();
            sprintRelease = CreateSprint(1);
            sprintList = new List<ISprint>();
            sprintList.Add(CreateSprint(2));
            sprintList.Add(CreateSprint(3));
        }

        private Backlog CreateBacklog()
        {
            // Create backlog
            backlog = new Backlog();
            // Create Items
            BacklogItem backlogitem = new BacklogItem("Item 1", new DateTime());
            backlogitem.AddActivity(new Activity("Create Homepage", false));
            backlogitem.AddActivity(new Activity("Create About Page", false));
            backlogitem.AddActivity(new Activity("Create Contact Page", false));
            backlogitem.AddDeveloper(new Developer("Jelle Stark"));

            BacklogItem backlogitem1 = new BacklogItem("Item 2", new DateTime());
            backlogitem.AddActivity(new Activity("Insert Patterns", false));
            backlogitem.AddDeveloper(new Developer("Jelle Stark"));

            BacklogItem backlogitem2 = new BacklogItem("Item 3", new DateTime());
            backlogitem.AddActivity(new Activity("Create Database", false));
            backlogitem.AddDeveloper(new Developer("Jelle Stark"));

            // Add Items
            backlog.AddBackLogItem(backlogitem);
            backlog.AddBackLogItem(backlogitem1);
            backlog.AddBackLogItem(backlogitem2);

            return backlog;
        }

        private ISprint CreateSprint(int number)
        {
            return new ReleaseSprint(backlog, number, "TestSprint");
        }

    }
}
