﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public interface IRepository
    {
        public string Source();
        public bool HasConnection();
        public Backlog GetBacklog();
        public List<ISprint> GetSprints();
    }
}
