﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class LocalDataRepository : IRepository
    {
        private LocalStorage localstorage;

        public LocalDataRepository()
        {
            localstorage = new LocalStorage();           
        }

        public Backlog GetBacklog()
        {
            return localstorage.backlog;
        }

        public List<ISprint> GetSprints()
        {
            return localstorage.sprintList;
        }

        public bool HasConnection()
        {
            return true;
        }

        public string Source()
        {
            return "This is the LocalData Repository Source";
        }
    }
}
