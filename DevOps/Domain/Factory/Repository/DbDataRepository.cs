﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class DbDataRepository : IRepository
    {
        public Backlog GetBacklog()
        {
            return null;
        }

        public List<ISprint> GetSprints()
        {
            return null;
        }

        public bool HasConnection()
        {
            return false;
        }

        public string Source()
        {
            return "This is the Database repository Source";
        }
    }
}
