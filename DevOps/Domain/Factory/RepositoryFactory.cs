﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class RepositoryFactory
    {
        public IRepository GetRepository(DATABASE repo)
        {
            if (repo.Equals(DATABASE.Database))
                return new DbDataRepository();

            else if(repo.Equals(DATABASE.FileData))
                return new FileDataRepository();

            else if (repo.Equals(DATABASE.Local))
                return new LocalDataRepository();

            return null;
        }
    }
}
