﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class LeadDeveloper : User
    {
        public LeadDeveloper(string name)
        {
            base.Name = name;
        }

        public void ChangeFaseBacklogItem(BacklogItem backlogItem, Fases fase)
        {
            if (fase.CurrentFase() == "Done" || fase.CurrentFase() == "ReadyForTesting")
            {
                backlogItem.ChangeFase(fase);
            }
        }
    }
}
