﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class User
    {
        public string Name;
        public int errorCount {get; set;} = 0;
        
        public void Notify(string message)
        {
            Console.WriteLine("User: {0} recieved the message: {1}", Name, message);
        }

        public void SendMessageToThread(string message, Thread thread)
        {
            thread.Add(new Message(message));
        }

        public void CreateThread(string message, BacklogItem backlogItem)
        {
            backlogItem.AddThread(message);
        }

        public BacklogItem CreateBackLogItem(Backlog backlog, string name)
        {
            ErrorMessage();
            return null;
        }

        public void ChangeFaseBacklogItem(BacklogItem backlogItem, Fases fase)
        {
            ErrorMessage();
        }

        public Activity CreateActivity(string name, BacklogItem backlogItem, bool status = false)
        {
            ErrorMessage();
            return null;
        }

        public void ChangeActivityStatus(Activity activity, bool status)
        {
            ErrorMessage();
        }

        public void AddDeveloperToBackLogItem(Developer user, BacklogItem backlogItem)
        {
            ErrorMessage();
        }

        public bool RunPipeline(ISprint sprint)
        {
            ErrorMessage();
            return false;
        }

        public void ErrorMessage()
        {
            errorCount++;
            Console.WriteLine("Your not supposed to do that");
        }
    }
}
