﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class ProductOwner : User
    {
        public ProductOwner(string name)
        {
            base.Name = name;
        }
    }
}
