﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Tester : User
    {
        public Tester(string name)
        {
            base.Name = name;
        }

        public void ChangeFaseBacklogItem(BacklogItem backlogItem, Fases fase)
        {
            if (fase.CurrentFase() == "Testing" || fase.CurrentFase() == "Tested")
            {
                backlogItem.ChangeFase(fase);
            }

            if (fase.CurrentFase() == "ToDo")
            {
                backlogItem.ChangeFase(fase);
                Notification.instance.NotifyAllScrummasters();
            }
        }
    }
}
