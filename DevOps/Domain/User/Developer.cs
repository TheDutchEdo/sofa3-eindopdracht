﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Developer : User
    {
        public Developer(string name)
        {
            base.Name = name;
        }
        public void ChangeActivityStatus(Activity activity, bool status)
        {
            activity.ChangeStatus(status);
        }
        public void ChangeFaseBacklogItem(BacklogItem backlogItem, Fases fase)
        {
            if (fase.CurrentFase() == "ReadyForTesting" || fase.CurrentFase() == "Doing")
            {
                backlogItem.ChangeFase(fase);
            }
        }
    }
}
