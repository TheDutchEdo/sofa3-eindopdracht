﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Domain
{
    public class Scrummaster : User
    {
        public Scrummaster(string name)
        {
            base.Name = name;
        }

        public BacklogItem CreateBackLogItem(Backlog backlog, string name)
        {
            BacklogItem item = new BacklogItem(name, new DateTime());
            backlog.AddBackLogItem(item);
            return item;
        }
        public void ChangeFaseBacklogItem(BacklogItem backlogItem, Fases fase)
        {
            backlogItem.ChangeFase(fase);
        }

        public Activity CreateActivity(string name, BacklogItem backlogItem, bool status = false)
        {
            Activity activity = new Activity(name, status);
            backlogItem.AddActivity(activity);
            return activity;
        }

        public void AddDeveloperToBackLogItem(Developer user, BacklogItem backlogItem)
        {
            backlogItem.AddDeveloper(user);
        }

        public bool RunPipeline(ISprint sprint)
        {
            return sprint.RunPipeline();
        }
    }
}
