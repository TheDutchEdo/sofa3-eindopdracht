﻿namespace DevOps.Domain
{
    interface Observable
    {
        public void AddUser(Observer observer);
        public void AddDeveloper(Observer observer);
        public void AddTester(Observer observer);
        public void AddScrummaster(Observer observer);
        public void AddProductowner(Observer observer);
        public void AddLeaddeveloper(Observer observer);
        public bool NotifyAll();
        public bool NotifyAllTesters();
        public bool NotifyAllScrummasters();
        public bool NotifyAllProductowner();
        public bool NotifyAllLeaddeveloper();
        public bool NotifyAllDevelopers();
        public void RemoveUser(Observer observer);
        public void RemoveTester(Observer observer);
        public void RemoveScrummaster(Observer observer);
        public void RemoveProductowner(Observer observer);
        public void RemoveDeveloper(Observer observer);
        public void RemoveLeaddeveloper(Observer observer);
    }
}
