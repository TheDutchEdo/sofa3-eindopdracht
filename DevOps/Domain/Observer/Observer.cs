﻿namespace DevOps.Domain
{
    public interface Observer
    {
        void Notify();
    }
}
