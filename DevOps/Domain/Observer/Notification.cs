﻿using System;
using System.Collections.Generic;

namespace DevOps.Domain
{
    public class Notification : Observable
    {
        public static Notification instance;

        public Notification()
        {
            instance = this;
        }

        public List<Observer> users = new List<Observer>();
        public List<Observer> testers = new List<Observer>();
        public List<Observer> developers = new List<Observer>();
        public List<Observer> scrummasters = new List<Observer>();
        public List<Observer> productowners = new List<Observer>();
        public List<Observer> leaddevelopers = new List<Observer>();
        public void AddUser(Observer observer)
        {
            users.Add(observer);
        }

        public void AddDeveloper(Observer observer)
        {
            developers.Add(observer);
        }

        public void AddTester(Observer observer)
        {
            testers.Add(observer);
        }

        public void AddScrummaster(Observer observer)
        {
            scrummasters.Add(observer);
        }
        public void AddProductowner(Observer observer)
        {
            productowners.Add(observer);
        }

        public void AddLeaddeveloper(Observer observer)
        {
            leaddevelopers.Add(observer);
        }

        public bool NotifyAll()
        {
            foreach (var observer in users)
            {
                observer.Notify();
            }
            return true;
        }

        public bool NotifyAllTesters()
        {
            foreach (var observer in testers)
            {
                observer.Notify();
            }
            return true;
        }

        public bool NotifyAllScrummasters()
        {
            foreach (var observer in scrummasters)
            {
                observer.Notify();
            }
            return true;
        }

        public bool NotifyAllProductowner()
        {
            foreach (var observer in productowners)
            {
                observer.Notify();
            }
            return true;
        }

        public bool NotifyAllLeaddeveloper()
        {
            foreach (var observer in scrummasters)
            {
                observer.Notify();
            }
            return true;
        }

        public bool NotifyAllDevelopers()
        {
            foreach (var observer in developers)
            {
                observer.Notify();
            }
            return true;
        }

        public void RemoveUser(Observer observer)
        {
            users.Remove(observer);
        }

        public void RemoveTester(Observer observer)
        {
            testers.Remove(observer);
        }

        public void RemoveScrummaster(Observer observer)
        {
            scrummasters.Remove(observer);
        }

        public void RemoveProductowner(Observer observer)
        {
            productowners.Remove(observer);
        }

        public void RemoveDeveloper(Observer observer)
        {
            developers.Remove(observer);
        }

        public void RemoveLeaddeveloper(Observer observer)
        {
            leaddevelopers.Remove(observer);
        }
    }
}
