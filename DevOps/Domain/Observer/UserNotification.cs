﻿using System;

namespace DevOps.Domain
{
    public class UserNotification : Observer
    {
        private User user;
        public UserNotification(User user)
        {
            this.user = user;
        }    

        public void Notify()
        {
            user.Notify("This is a test notification");
        }
    }
}
