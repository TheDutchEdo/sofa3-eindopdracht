﻿using System;
using System.Collections.Generic;

namespace DevOps.Domain
{
    public class Thread : Forum
    {
        private List<Forum> children = new List<Forum>();
        private BacklogItem backlogItem;
        // Constructor
        public Thread(string name, BacklogItem backlogItem)
            : base(name)
        {
            this.backlogItem = backlogItem;
        }
        public override void Add(Forum component)
        {
            if (backlogItem.GetFase().CurrentFase() != "Done")
            {
                children.Add(component);
                Notification.instance.NotifyAll();
            }
        }
        public override void Remove(Forum component)
        {
            children.Remove(component);
        }

        public override List<Forum> GetChildren()
        {
            return children;
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + name);
            // Recursively display child nodes
            foreach (Forum component in children)
            {
                component.Display(depth + 2);
            }
        }
    }
}
