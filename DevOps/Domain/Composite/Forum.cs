﻿using System.Collections.Generic;

namespace DevOps.Domain
{
    public abstract class Forum
    {
        public string name { get; private set; }
        // Constructor
        public Forum(string name)
        {
            this.name = name;
        }
        public abstract void Add(Forum c);
        public abstract void Remove(Forum c);
        public abstract void Display(int depth);
        public abstract List<Forum> GetChildren();
    }
}
