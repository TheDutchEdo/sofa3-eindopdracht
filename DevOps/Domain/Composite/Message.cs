﻿using System;
using System.Collections.Generic;

namespace DevOps.Domain
{
    public class Message : Forum
    {
        // Constructor
        public Message(string name)
            : base(name)
        {
        }

        public override void Add(Forum c)
        {
            Console.WriteLine("Cannot add to a leaf");
        }

        public override void Remove(Forum c)
        {
            Console.WriteLine("Cannot remove from a leaf");
        }
        public override void Display(int depth)
        {
            Console.WriteLine(new String('-', depth) + name);
        }

        public override List<Forum> GetChildren()
        {
            Console.WriteLine("Doesnt have children!");
            return null;
        }
    }
}
