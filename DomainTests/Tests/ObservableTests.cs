﻿using DevOps.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DomainTests.Tests
{
    public class ObservableTests
    {
        [Fact]
        public void CreateNotificationService()
        {
            Notification notification = new Notification();

            Assert.Equal(notification, Notification.instance);
        }

        [Fact]
        public void TestAddUserFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddUser(observer);

            Assert.NotEmpty(notification.users);
        }

        [Fact]
        public void TestAddTesterFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddTester(observer);

            Assert.NotEmpty(notification.testers);
        }
        [Fact]
        public void TestAddDevelopersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddDeveloper(observer);

            Assert.NotEmpty(notification.developers);
        }

        [Fact]
        public void TestAddScrummasterFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddScrummaster(observer);

            Assert.NotEmpty(notification.scrummasters);
        }

        [Fact]
        public void TestAddProductownersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddProductowner(observer);

            Assert.NotEmpty(notification.productowners);
        }

        [Fact]
        public void TestAddLeaddevelopersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);

            notification.AddLeaddeveloper(observer);

            Assert.NotEmpty(notification.leaddevelopers);
        }

        [Fact]
        public void TestRemoveUserFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);
            notification.AddUser(observer);

            notification.RemoveUser(observer);

            Assert.Empty(notification.users);
        }

        [Fact]
        public void TestRemoveDevelopersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);
            notification.AddDeveloper(observer);

            notification.RemoveDeveloper(observer);

            Assert.Empty(notification.developers);
        }

        [Fact]
        public void TestRemoveScrummastersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);
            notification.AddScrummaster(observer);

            notification.RemoveScrummaster(observer);

            Assert.Empty(notification.scrummasters);
        }

        [Fact]
        public void TestRemoveProductownersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);
            notification.AddProductowner(observer);

            notification.RemoveProductowner(observer);

            Assert.Empty(notification.productowners);
        }

        [Fact]
        public void TestRemoveLeaddevelopersFromuser()
        {
            Notification notification = new Notification();
            User user = new User();
            Observer observer = new UserNotification(user);
            notification.AddLeaddeveloper(observer);

            notification.RemoveLeaddeveloper(observer);

            Assert.Empty(notification.leaddevelopers);
        }

        [Fact]
        public void TestNotifyAll()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAll();

            Assert.True(result);
        }

        [Fact]
        public void TestNotifyAllTesters()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAllTesters();

            Assert.True(result);
        }
        [Fact]
        public void TestNotifyAllScrummasters()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAllScrummasters();

            Assert.True(result);
        }
        [Fact]
        public void TestNotifyAllProductOwner()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAllProductowner();

            Assert.True(result);
        }
        [Fact]
        public void TestNotifyAllLeaddeveloper()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAllLeaddeveloper();

            Assert.True(result);
        }
        [Fact]
        public void TestNotifyAllDevelopers()
        {
            Notification notification = new Notification();

            bool result = notification.NotifyAllDevelopers();

            Assert.True(result);
        }

        [Fact]
        public void TestNotifyAllDevelopersWithList()
        {
            Notification notification = new Notification();
            notification.AddDeveloper(new UserNotification(new User()));

            bool result = notification.NotifyAllDevelopers();

            Assert.True(result);
            Assert.NotEmpty(notification.developers);
        }

        [Fact]
        public void TestNotifyAllWithList()
        {
            Notification notification = new Notification();
            notification.AddUser(new UserNotification(new User()));

            bool result = notification.NotifyAll();

            Assert.True(result);
            Assert.NotEmpty(notification.users);
        }

        [Fact]
        public void TestNotifyAllTestersWithList()
        {
            Notification notification = new Notification();
            notification.AddTester(new UserNotification(new User()));

            bool result = notification.NotifyAllTesters();

            Assert.True(result);
            Assert.NotEmpty(notification.testers);
        }
        [Fact]
        public void TestNotifyAllScrummastersWithList()
        {
            Notification notification = new Notification();
            notification.AddScrummaster(new UserNotification(new User()));

            bool result = notification.NotifyAllScrummasters();

            Assert.True(result);
            Assert.NotEmpty(notification.scrummasters);
        }
        [Fact]
        public void TestNotifyAllProductOwnerWithList()
        {
            Notification notification = new Notification();
            notification.AddProductowner(new UserNotification(new User()));

            bool result = notification.NotifyAllProductowner();

            Assert.True(result);
            Assert.NotEmpty(notification.productowners);
        }
        [Fact]
        public void TestNotifyAllLeaddeveloperWithList()
        {
            Notification notification = new Notification();
            notification.AddLeaddeveloper(new UserNotification(new User()));

            bool result = notification.NotifyAllLeaddeveloper();

            Assert.True(result);
            Assert.NotEmpty(notification.leaddevelopers);
        }
    }
}
