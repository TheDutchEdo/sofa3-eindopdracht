﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using DevOps.Domain;

namespace Tests
{
    public class StatusTests
    {
        [Fact]
        public void StatusUnfinishedGivesUnFinished()
        {
            IStatus status = new Unfinished();

            string thestatus = status.GetStatus();

            Assert.Equal(STATUS.Unfinished.ToString(), thestatus);
        }

        [Fact]
        public void StatusfinishedGivesFinished()
        {
            IStatus status = new Finished();

            string thestatus = status.GetStatus();

            Assert.Equal(STATUS.Finished.ToString(), thestatus);
        }

        [Fact]
        public void StatusChangedTofinishedGivesfinished()
        {
            IStatus status = new Unfinished();

            status = new Finished();
            string thestatus = status.GetStatus();

            Assert.Equal(STATUS.Finished.ToString(), thestatus);
        }

        [Fact]
        public void StatusChangedTounfinishedGivesunfinished()
        {
            IStatus status = new Finished();

            status = new Unfinished();
            string thestatus = status.GetStatus();

            Assert.Equal(STATUS.Unfinished.ToString(), thestatus);
        }

        [Fact]
        public void StatustoPlannedInGivesPlannedin()
        {
            IStatus status = new PlannedIn();

            string thestatus = status.GetStatus();

            Assert.Equal(STATUS.PlannedIn.ToString(), thestatus);
        }
    }
}
