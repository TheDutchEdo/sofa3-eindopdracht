﻿using DevOps.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class StrategyTests
    {
        [Fact]
        public void TestCreateRapportResultIsRapport()
        {
            Backlog backlog = new Backlog();
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            Rapport rapport = new Rapport(new ExportToPdf(), sprint);

            Assert.NotNull(rapport);
        }

        [Fact]
        public void TestSetStrategyRapportResultIsChangedStrategy()
        {
            Backlog backlog = new Backlog();
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");
            ExportStrategy exportStrategy = new ExportToPdf();
            ExportStrategy exportStrategy2 = new ExportToPng();

            Rapport rapport = new Rapport(exportStrategy, sprint);
            rapport.SetStrategy(exportStrategy2);

            Assert.NotEqual(exportStrategy, exportStrategy2);
        }

        //[Fact]
        //public void TestExportPDFRapportResultAnPFDExport()
        //{
        //    Backlog backlog = new Backlog();
        //    ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");
        //    ExportStrategy exportStrategy = new ExportToPdf();
        //    Rapport rapport = new Rapport(exportStrategy, sprint);
            
        //    rapport.Export();

        //    Assert.True(File.Exists("export-1.pdf"));
        //}

        [Fact]
        public void TestExportPNFRapportResultAnPNGExport()
        {
            Backlog backlog = new Backlog();
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");
            ExportStrategy exportStrategy = new ExportToPng();
            Rapport rapport = new Rapport(exportStrategy, sprint);

            rapport.Export();

            Assert.True(File.Exists("export-1.png"));
        }
    }
}
