﻿using DevOps.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class BacklogTests
    {
        [Fact]
        public void TestCreateActivityResultCreatedActivity()
        {
            Activity activity = new Activity("Test activity", false);

            Assert.Equal("Test activity", activity.name);
            Assert.NotNull(activity);
            Assert.False(activity.GetStatus());
        }

        [Fact]
        public void TestChangeStatusActivityResultStatusChanged()
        {
            Activity activity = new Activity("Test activity", false);

            activity.ChangeStatus(true);

            Assert.True(activity.GetStatus());
        }

        [Fact]
        public void TestGetStatusActivityResultFalse()
        {
            Activity activity = new Activity("Test activity", false);

            bool result = activity.GetStatus();     

            Assert.False(result);
        }

        [Fact]
        public void TestCreateBacklogResultCreatedBacklog()
        {
            Backlog backlog = new Backlog();

            Assert.NotNull(backlog);
            Assert.Empty(backlog.GetBackLogItems());
        }

        [Fact]
        public void TestAddBacklogItemResultAddedBacklogItem()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlog.AddBackLogItem(backlogItem);

            Assert.NotNull(backlog);
            Assert.NotEmpty(backlog.GetBackLogItems());
        }

        [Fact]
        public void TestGetBacklogItemsResultReturnBacklogItems()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            List<BacklogItem> list = backlog.GetBackLogItems();

            Assert.NotNull(list);
            Assert.NotEmpty(list);
        }

        [Fact]
        public void TestLockBacklogItemResultResultLockedBacklogitems()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            backlog.LockBackLogItem();

            Assert.True(backlog.GetBackLogItems()[0].isLocked);
        }

        [Fact]
        public void TestCreateBacklogItemResultCreatedBacklogItem()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            Assert.NotNull(backlogItem);
            Assert.False(backlogItem.isLocked);
        }

        [Fact]
        public void TestBacklotitemChangeNameWhenisNotLockedResultNameChanged()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlogItem.Changename("Backlog item2");

            Assert.Equal("Backlog item2", backlogItem.name);
            Assert.False(backlogItem.isLocked);
        }

        [Fact]
        public void TestBacklotitemChangeNameWhenisLockedResultNameChanged()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlogItem.LockBacklogItem();
            backlogItem.Changename("Backlog item2");

            Assert.Equal("Backlog item", backlogItem.name);
            Assert.True(backlogItem.isLocked);
        }

        [Fact]
        public void TestGetActivitiesWithEmpytListResultReturnActivites()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            List<Activity> list = backlogItem.GetActivities();

            Assert.Empty(list);
        }

        [Fact]
        public void TestGetActivitiesWithActivityResultReturnActivites()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddActivity(new Activity("Test activity", false));

            List<Activity> list = backlogItem.GetActivities();

            Assert.NotEmpty(list);
        }

        [Fact]
        public void TestBacklotitemAddACtivityWhenisNotLockedResultActivityAdded()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlogItem.AddActivity(new Activity("Test activity", false));

            Assert.NotEmpty(backlogItem.GetActivities());
            Assert.False(backlogItem.isLocked);
        }

        [Fact]
        public void TestBacklotitemAddACtivityWhenisLockedResultACtivityNotAdded()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlogItem.LockBacklogItem();
            backlogItem.AddActivity(new Activity("Test activity", false));

            Assert.Empty(backlogItem.GetActivities());
            Assert.True(backlogItem.isLocked);
        }

        [Fact]
        public void TestBacklotitemAddDeveloperWithEnoughActivitiesResultDeveloperAdded()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlogItem.AddActivity(new Activity("Test activity", false));

            backlogItem.AddDeveloper(new Developer("Edo van Voorst"));

            Assert.NotEmpty(backlogItem.GetDevelopers());
            Assert.NotEmpty(backlogItem.GetActivities());
        }

        [Fact]
        public void TestBacklotitemAddDeveloperWithNotEnoughActivitiesResultDeveloperNotAdded()
        {
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            backlogItem.AddDeveloper(new Developer("Edo van Voorst"));

            Assert.Empty(backlogItem.GetDevelopers());
            Assert.Empty(backlogItem.GetActivities());
        }

        [Fact]
        public void TestGetDevelopersWithEmpytListResultReturnDevelopers()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());

            List<Developer> list = backlogItem.GetDevelopers();

            Assert.Empty(list);
        }

        [Fact]
        public void TestGetDevelopersWithDevelopersResultReturnDevelopers()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddActivity(new Activity("Test activity", false));
            backlogItem.AddDeveloper(new Developer("Edo van Voorst"));

            List<Developer> list = backlogItem.GetDevelopers();

            Assert.NotEmpty(list);
        }

        [Fact]
        public void TestChangeFaseToDoneWithActivityNotDoneResultBacklogItemFaseIsNotDone()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddActivity(new Activity("Test activity", false));
            Fases fase = new StateDone();

            backlogItem.ChangeFase(fase);

            Assert.Equal(fase, backlogItem.GetFase());
        }

        [Fact]
        public void TestChangeFaseToDoneWithActivityDoneResultBacklogItemFaseIsDone()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddActivity(new Activity("Test activity", true));
            Fases fase = new StateDone();

            backlogItem.ChangeFase(fase);

            Assert.Equal(fase, backlogItem.GetFase());
        }

        [Fact]
        public void TestChangeFaseToReadyForTestingResultBacklogItemFaseIsReadyForTesting()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Fases fase = new StateReadyForTesting();

            backlogItem.ChangeFase(fase);

            Assert.Equal(fase, backlogItem.GetFase());
        }

        [Fact]
        public void TestChangeFaseToDoingResultBacklogItemFaseIsDoing()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Fases fase = new StateDoing();

            backlogItem.ChangeFase(fase);

            Assert.Equal(fase, backlogItem.GetFase());
        }

        [Fact]
        public void TestGetCurrentFaseResultDoing()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Fases fase = new StateDoing();
            backlogItem.ChangeFase(fase);

            Assert.Equal(fase, backlogItem.GetFase());
        }

        [Fact]
        public void TestGetThreadResultThreadFound()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddThread("Test");

            Thread thread = backlogItem.GetThread("Test");

            Assert.NotNull(thread);
        }

        [Fact]
        public void TestAddMessages()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            backlogItem.AddThread("Test");
            Thread thread = backlogItem.GetThread("Test");

            backlogItem.AddMessages("Test2");

            Assert.NotNull(thread);
        }

        [Fact]
        public void TestLockBacklogitem()
        {
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("Backlog item", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            backlogItem.LockBacklogItem();

            Assert.True(backlogItem.isLocked);
        }

    }
}
