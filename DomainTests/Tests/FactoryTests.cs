﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using DevOps.Domain;

namespace Tests
{
    public class FactoryTests
    {
        [Fact]
        public void RepositoryFactoryGivesLocalDataRepository()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Local);

            Assert.Equal("This is the LocalData Repository Source", repository.Source());

        }

        [Fact]
        public void RepositoryFactoryGivesDbDataRepository()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Database);

            Assert.Equal("This is the Database repository Source", repository.Source());
        }

        [Fact]
        public void RepositoryFactoryGivesFileDataRepository()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.FileData);

            Assert.Equal("This is the filedata Repository Source", repository.Source());
        }

        [Fact]
        public void RepositoryFactoryGivesLocalDataRepositoryChecksConnection()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Local);

            Assert.True(repository.HasConnection());

        }

        [Fact]
        public void RepositoryFactoryGivesDbDataRepositoryChecksConnection()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Database);

            Assert.False(repository.HasConnection());
        }

        [Fact]
        public void RepositoryFactoryGivesFileDataRepositoryChecksConnection()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.FileData);

            Assert.False(repository.HasConnection());
        }

        [Fact]
        public void RepositoryFactoryGivesLocalDataRepositoryGetBacklog()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Local);

            Assert.NotNull(repository.GetBacklog());

        }

        [Fact]
        public void RepositoryFactoryGivesDbDataRepositoryGetBacklog()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Database);

            Assert.Null(repository.GetBacklog());
        }

        [Fact]
        public void RepositoryFactoryGivesFileDataRepositoryGetBacklog()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.FileData);

            Assert.Null(repository.GetBacklog());
        }

        [Fact]
        public void RepositoryFactoryGivesLocalDataRepositoryGetSprints()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Local);

            Assert.NotNull(repository.GetSprints());

        }

        [Fact]
        public void RepositoryFactoryGivesDbDataRepositoryGetSprints()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.Database);

            Assert.Null(repository.GetSprints());
        }

        [Fact]
        public void RepositoryFactoryGivesFileDataRepositoryGetSprints()
        {
            RepositoryFactory repositoryFactory = new RepositoryFactory();
            IRepository repository = repositoryFactory.GetRepository(DATABASE.FileData);

            Assert.Null(repository.GetSprints());
        }


    }
}
