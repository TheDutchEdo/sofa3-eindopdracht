﻿using DevOps.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class UserTests
    {
        [Fact]
        public void CanCreateDeveloperReturnsUserDeveloper()
        {
            User developer = new Developer("Edo van Voorst");
            Assert.NotNull(developer);
        }

        [Fact]
        public void CanCreateLeadDeveloperReturnsUserLeadDeveloper()
        {
            User developer = new LeadDeveloper("Edo van Voorst");
            Assert.NotNull(developer);
        }

        [Fact]
        public void CanCreateProductOwnerReturnsUserProductOwner()
        {
            User developer = new ProductOwner("Edo van Voorst");
            Assert.NotNull(developer);
        }

        [Fact]
        public void CanCreateScrummasterReturnsUserScrummaster()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Assert.NotNull(developer);
        }

        [Fact]
        public void CanCreateTesterReturnsUserTester()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Assert.NotNull(developer);
        }

        [Fact]
        public void CanCreateDeveloperReturnsUserDeveloperWithSubmittedName()
        {
            User developer = new Developer("Edo van Voorst");
            Assert.Equal("Edo van Voorst", developer.Name);
        }

        [Fact]
        public void CanCreateLeadDeveloperReturnsUserLeadDeveloperWithSubmittedName()
        {
            User developer = new LeadDeveloper("Edo van Voorst");
            Assert.Equal("Edo van Voorst", developer.Name);
        }

        [Fact]
        public void CanCreateProductOwnerReturnsUserProductOwnerWithSubmittedName()
        {
            User developer = new ProductOwner("Edo van Voorst");
            Assert.Equal("Edo van Voorst", developer.Name);
        }

        [Fact]
        public void CanCreateScrummasterReturnsUserScrummasterWithSubmittedName()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Assert.Equal("Edo van Voorst", developer.Name);
        }

        [Fact]
        public void CanCreateTesterReturnsUserTesterWithSubmittedName()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Assert.Equal("Edo van Voorst", developer.Name);
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateBackLogItemResultTheDeveloperIsNotSupposedToDo()
        {
            User developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();

            developer.CreateBackLogItem(backlog, "TestItem");
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateBackLogItemResultTheLeadDeveloperIsNotSupposedToDo()
        {
            User developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();

            developer.CreateBackLogItem(backlog, "TestItem");
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateBackLogItemResultProductOwnerIsNotSupposedToDo()
        {
            User developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();

            developer.CreateBackLogItem(backlog, "TestItem");
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateBackLogItemResultTesterIsNotSupposedToDo()
        {
            User developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();

            developer.CreateBackLogItem(backlog, "TestItem");
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void CreatesNoErrorMessagesWithTheFunctionCreateBackLogItemResultScrummasterIsSupposedToDo()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();

            developer.CreateBackLogItem(backlog, "TestItem");
            Assert.Equal(0, developer.errorCount);
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionChangeFaseBacklogItemResultTheDeveloperIsSupposedToDo()
        {
            Developer developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDone());
            Assert.Equal(0, developer.errorCount);
            Assert.Equal("ToDo", new StateToDo().CurrentFase());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionChangeFaseBacklogItemResultTheLeadDeveloperIsSupposedToDo()
        {
            LeadDeveloper developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDone());
            Assert.Equal(0, developer.errorCount);
            Assert.Equal("ToDo", new StateToDo().CurrentFase());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionChangeFaseBacklogItemResultTheProductOwnerIsNotSupposedToDo()
        {
            ProductOwner developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDone());
            Assert.Equal(1, developer.errorCount);
            Assert.Equal("ToDo", new StateToDo().CurrentFase());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionChangeFaseBacklogItemResultTheTesterIsSupposedToDo()
        {
            Tester developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDone());
            Assert.Equal(0, developer.errorCount);
            Assert.Equal("ToDo", new StateToDo().CurrentFase());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionChangeFaseBacklogItemResultTheScrummasterIsSupposedToDo()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.ChangeFaseBacklogItem(backlogItem, new StateDone());
            Assert.Equal(0, developer.errorCount);
            Assert.Equal("Done", new StateDone().CurrentFase());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateActivityResultTheDeveloperIsNotSupposedToDo()
        {
            Developer developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateActivity("TestActivity", backlogItem);
            Assert.Equal(1, developer.errorCount);
            Assert.Empty(backlogItem.GetActivities());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateActivityResultTheLeadDeveloperIsNotSupposedToDo()
        {
            LeadDeveloper developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateActivity("TestActivity", backlogItem);
            Assert.Equal(1, developer.errorCount);
            Assert.Empty(backlogItem.GetActivities());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateActivityResultProductOwnerIsNotSupposedToDo()
        {
            ProductOwner developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateActivity("TestActivity", backlogItem);
            Assert.Equal(1, developer.errorCount);
            Assert.Empty(backlogItem.GetActivities());
        }

        [Fact]
        public void CreatesErrorMessagesWithTheFunctionCreateActivityTesterIsNotSupposedToDo()
        {
            Tester developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateActivity("TestActivity", backlogItem);
            Assert.Equal(1, developer.errorCount);
            Assert.Empty(backlogItem.GetActivities());
        }

        [Fact]
        public void CreatesNoErrorMessagesWithTheFunctionCreateActivityResultScrummasterIsSupposedToDo()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateActivity("TestActivity", backlogItem);
            Assert.Equal(0, developer.errorCount);
            Assert.NotEmpty(backlog.GetBackLogItems());
            Assert.Equal("TestActivity", backlogItem.GetActivities()[0].name);
            Assert.NotEqual("abc", backlogItem.GetActivities()[0].name);
        }

        [Fact]
        public void ChangeActivityStatusAsProductOwnerResultHeIsNotSupposedToDoThat()
        {
            ProductOwner developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Activity activity = new Activity("TestActivity", false);
            backlogItem.AddActivity(activity);

            developer.ChangeActivityStatus(activity, true);

            Assert.Equal(1, developer.errorCount);
            Assert.Equal("TestActivity", activity.name);
            Assert.False(activity.GetStatus());
        }

        [Fact]
        public void ChangeActivityStatusAsScrummasterResultHeIsNotSupposedToDoThat()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Activity activity = new Activity("TestActivity", false);
            backlogItem.AddActivity(activity);

            developer.ChangeActivityStatus(activity, true);

            Assert.Equal(1, developer.errorCount);
            Assert.Equal("TestActivity", activity.name);
            Assert.False(activity.GetStatus());
        }
        [Fact]
        public void ChangeActivityStatusAsTesterResultHeIsNotSupposedToDoThat()
        {
            Tester developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Activity activity = new Activity("TestActivity", false);
            backlogItem.AddActivity(activity);

            developer.ChangeActivityStatus(activity, true);

            Assert.Equal(1, developer.errorCount);
            Assert.Equal("TestActivity", activity.name);
            Assert.False(activity.GetStatus());
        }

        [Fact]
        public void ChangeActivityStatusAsLeaddeveloperResultHeIsNotSupposedToDoThat()
        {
            LeadDeveloper developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Activity activity = new Activity("TestActivity", false);
            backlogItem.AddActivity(activity);

            developer.ChangeActivityStatus(activity, true);

            Assert.Equal(1, developer.errorCount);
            Assert.Equal("TestActivity", activity.name);
            Assert.False(activity.GetStatus());
        }

        [Fact]
        public void ChangeActivityStatusAsDeveloperResultHeIsSupposedToDoThat()
        {
            Developer developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Activity activity = new Activity("TestActivity", false);
            backlogItem.AddActivity(activity);

            developer.ChangeActivityStatus(activity, true);

            Assert.Equal(0, developer.errorCount);
            Assert.Equal("TestActivity", activity.name);
            Assert.True(activity.GetStatus());
        }

        [Fact]
        public void AddDeveloperToBackLogItemAsDeveloperResultHeIsNotSupposedToDoThat()
        {
            Developer developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Developer developer2 = new Developer("Jelle Stark");

            developer.AddDeveloperToBackLogItem(developer2, backlogItem);

            Assert.Empty(backlogItem.GetDevelopers());
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void AddDeveloperToBackLogItemAsLeadDeveloperResultHeIsNotSupposedToDoThat()
        {
            LeadDeveloper developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Developer developer2 = new Developer("Jelle Stark");

            developer.AddDeveloperToBackLogItem(developer2, backlogItem);

            Assert.Empty(backlogItem.GetDevelopers());
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void AddDeveloperToBackLogItemAsProductOwnerResultHeIsNotSupposedToDoThat()
        {
            ProductOwner developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Developer developer2 = new Developer("Jelle Stark");

            developer.AddDeveloperToBackLogItem(developer2, backlogItem);

            Assert.Empty(backlogItem.GetDevelopers());
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void AddDeveloperToBackLogItemAsTesterResultHeIsNotSupposedToDoThat()
        {
            Tester developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Developer developer2 = new Developer("Jelle Stark");

            developer.AddDeveloperToBackLogItem(developer2, backlogItem);

            Assert.Empty(backlogItem.GetDevelopers());
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void AddDeveloperToBackLogItemAsScrummasterResultHeIsSupposedToDoThat()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            Developer developer2 = new Developer("Jelle Stark");
            Activity activity = developer.CreateActivity("TestActivity", backlogItem);

            developer.AddDeveloperToBackLogItem(developer2, backlogItem);

            Assert.NotEmpty(backlogItem.GetDevelopers());
            Assert.Equal(0, developer.errorCount);
        }

        [Fact]
        public void TestErrorMessageResultErrorCount()
        {
            User developer = new Scrummaster("Edo van Voorst");

            developer.ErrorMessage();

            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void TestCreateThreadResultThread()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);

            developer.CreateThread("Test thread", backlogItem);

            Assert.NotNull(backlogItem.GetThread("Test thread"));
        }

        //[Fact]
        //public void TestAddMessageToThreadResultThreadWithMessages()
        //{
        //    Scrummaster developer = new Scrummaster("Edo van Voorst");
        //    Backlog backlog = new Backlog();
        //    BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
        //    developer.ChangeFaseBacklogItem(backlogItem, new StateDoing());
        //    backlog.AddBackLogItem(backlogItem);
        //    developer.CreateThread("Code coverage", backlogItem);
        //    Thread thread = backlogItem.GetThread("Code coverage");
        //    Message message = new Message("Test messages");

        //    thread.Add(message);

        //    Assert.NotNull(thread.GetChildren());
        //    Assert.Equal("Test messages", thread.GetChildren()[0].name);
        //}

        [Fact]
        public void TestAddMessageToMessagesResultMessagesNotAdded()
        {
            User developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            developer.CreateThread("Test thread", backlogItem);
            Message messages = new Message("Test messages");

            messages.Add(new Message("Test messages"));

            Assert.Null(messages.GetChildren());
        }

        [Fact]
        public void RunPipelineAsDeveloperResultHeIsNotSupposedToDoThat()
        {
            Developer developer = new Developer("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            bool result = developer.RunPipeline(sprint);

            Assert.False(result);
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void RunPipelineAsLeadDeveloperResultHeIsNotSupposedToDoThat()
        {
            LeadDeveloper developer = new LeadDeveloper("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            bool result = developer.RunPipeline(sprint);

            Assert.False(result);
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void RunPipelineAsProductOwnerResultHeIsNotSupposedToDoThat()
        {
            ProductOwner developer = new ProductOwner("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            bool result = developer.RunPipeline(sprint);

            Assert.False(result);
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void RunPipelineAsTesterResultHeIsNotSupposedToDoThat()
        {
            Tester developer = new Tester("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            bool result = developer.RunPipeline(sprint);

            Assert.False(result);
            Assert.Equal(1, developer.errorCount);
        }

        [Fact]
        public void RunPipelineAsScrummasterResultHeIsSupposedToDoThat()
        {
            Scrummaster developer = new Scrummaster("Edo van Voorst");
            Backlog backlog = new Backlog();
            BacklogItem backlogItem = new BacklogItem("TestItem", new DateTime());
            backlog.AddBackLogItem(backlogItem);
            ISprint sprint = new DeploymentSprint(backlog, 1, "Deploysprint");

            bool result = developer.RunPipeline(sprint);

            Assert.True(result);
            Assert.Equal(0, developer.errorCount);
        }
    }
}
