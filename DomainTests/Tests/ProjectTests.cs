﻿using DevOps.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class ProjectTests
    {
        [Fact]
        public void TestCreateProjectResultProjectCreated()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();

            Project project = new Project("Testproject", users, sprints);

            Assert.NotNull(project);
        }

        [Fact]
        public void TestAddUserToProjectResultUserAdded()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);
            User user = new Developer("Edo van Voorst");

            project.AddUser(user);

            Assert.NotNull(project.GetUsers()[0]);
            Assert.Equal("Edo van Voorst", project.GetUsers()[0].Name);
        }

        [Fact]
        public void TestGetUserFromProjectResultGotUser()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);
            User user = new Developer("Edo van Voorst");
            project.AddUser(user);

            List<User> gotusers = project.GetUsers();

            Assert.NotNull(gotusers);
            Assert.NotEmpty(gotusers);
        }

        [Fact]
        public void TestGetUserFromProjectResultEmptylist()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);

            List<User> gotusers = project.GetUsers();

            Assert.Empty(gotusers);
        }

        [Fact]
        public void TestAddSprintToProjectResultSprinitAdded()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);
            ISprint sprint = new ReleaseSprint(new Backlog(), 1, "Test");

            project.AddSprint(sprint);

            Assert.NotNull(project.GetSprints()[0]);
            Assert.Equal("Test", project.GetSprints()[0].GetName());
        }

        [Fact]
        public void TestGetSprintFromProjectResultGotSprint()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);
            ISprint sprint = new ReleaseSprint(new Backlog(), 1, "Test");
            project.AddSprint(sprint);

            List<ISprint> gotsprints = project.GetSprints();

            Assert.NotNull(gotsprints);
            Assert.NotEmpty(gotsprints);
        }

        [Fact]
        public void TestGetSprintFromProjectResultEmptylist()
        {
            List<User> users = new List<User>();
            List<ISprint> sprints = new List<ISprint>();
            Project project = new Project("Testproject", users, sprints);
            ISprint sprint = new ReleaseSprint(new Backlog(), 1, "Test");

            List<ISprint> gotsprints = project.GetSprints();

            Assert.Empty(gotsprints);
        }
    }
}
