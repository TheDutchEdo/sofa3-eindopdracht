﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using DevOps.Domain;

namespace Tests
{
    public class PipelineTests
    {
        [Fact]
        public void RunReleasePipelineGivesTrue()
        {
            bool expected = PipeLineRunner.RunPipeline(new ReleasePipeline());

            Assert.True(expected);
        }

        [Fact]
        public void RunDevelopmentPipelineGivesTrue()
        {
            bool expected = PipeLineRunner.RunPipeline(new DevelopmentPipeline());

            Assert.True(expected);
        }
    }
}
