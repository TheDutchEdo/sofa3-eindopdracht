﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using DevOps.Domain;

namespace Tests
{
    public class StateTests
    {
        [Fact]
        public void CreatingTodoStateGivesCorrectFase()
        {         
            Fases Todo = new StateToDo();

            Assert.Equal("ToDo", Todo.CurrentFase());
        }

        [Fact]
        public void CreatingDoingStateGivesCorrectFase()
        {
            Fases doing = new StateDoing();

            Assert.Equal("Doing", doing.CurrentFase());
        }

        [Fact]
        public void CreatingtestingStateGivesCorrectFase()
        {
            Fases testing = new StateTesting();

            Assert.Equal("Testing", testing.CurrentFase());
        }

        [Fact]
        public void CreatingDoneStateGivesCorrectFase()
        {
            Fases Done = new StateDone();

            Assert.Equal("Done", Done.CurrentFase());
        }

        [Fact]
        public void CreatingTestedStateGivesCorrectFase()
        {
            Fases tested = new StateTested();

            Assert.Equal("Tested", tested.CurrentFase());
        }

        [Fact]
        public void CreatingReadyForTestingStateGivesCorrectFase()
        {
            Fases readyfortesting = new StateReadyForTesting();

            Assert.Equal("ReadyForTesting", readyfortesting.CurrentFase());
        }
    }
}
