﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using DevOps.Domain;

namespace Tests
{
    public class SprintTests
    {
        [Fact]
        public void ReviewSprintWeeknumberGivesCorrectWeeknumber()
        {
            Backlog backlog = new Backlog();
            int weeknumber = 2;
            ISprint reviewsprint = new ReviewSprint(backlog, weeknumber, "Testsprint");

            int getweeknumber = reviewsprint.GetWeeknumber();

            Assert.Equal(weeknumber, getweeknumber);
        }

        [Fact]
        public void DeploymentSprintWeeknumberGivesCorrectWeeknumber()
        {
            Backlog backlog = new Backlog();
            int weeknumber = 2;
            ISprint Deploymentsprint = new DeploymentSprint(backlog, weeknumber, "Testsprint");

            int getweeknumber = Deploymentsprint.GetWeeknumber();

            Assert.Equal(weeknumber, getweeknumber);
        }

        [Fact]
        public void ReleaseSprintWeeknumberGivesCorrectWeeknumber()
        {
            Backlog backlog = new Backlog();
            int weeknumber = 2;
            ISprint releasesprint = new ReleaseSprint(backlog, weeknumber, "Testsprint");

            int getweeknumber = releasesprint.GetWeeknumber();

            Assert.Equal(weeknumber, getweeknumber);
        }

        [Fact]
        public void ReviewSprintbacklogGivesCorrectbacklog()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            Backlog getbacklog = reviewsprint.GetBacklog();

            Assert.Equal(backlog, getbacklog);
        }

        [Fact]
        public void DeploymentSprintbacklogGivesCorrectbacklog()
        {
            Backlog backlog = new Backlog();
            ISprint Deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");

            Backlog getbacklog = Deploymentsprint.GetBacklog();

            Assert.Equal(backlog, getbacklog);
        }

        [Fact]
        public void ReleaseSprintbacklogGivesCorrectbacklog()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");

            Backlog getbacklog = releasesprint.GetBacklog();

            Assert.Equal(backlog, getbacklog);
        }

        [Fact]
        public void ReviewSprintRapportGivesCorrectRapport()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");
            Rapport rapport = new Rapport(new ExportToPdf(), reviewsprint);
            reviewsprint.AddRapport(rapport);

            Rapport getRapport = reviewsprint.GetRapport();

            Assert.Equal(rapport, getRapport);
        }

        [Fact]
        public void DeploymentSprintRapportGivesCorrectRapport()
        {
            Backlog backlog = new Backlog();
            ISprint deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");
            Rapport rapport = new Rapport(new ExportToPdf(), deploymentsprint);
            deploymentsprint.AddRapport(rapport);

            Rapport getRapport = deploymentsprint.GetRapport();

            Assert.Equal(rapport, getRapport);
        }

        [Fact]
        public void ReleaseSprintRapportGivesCorrectRapport()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");
            Rapport rapport = new Rapport(new ExportToPdf(), releasesprint);
            releasesprint.AddRapport(rapport);

            Rapport getRapport = releasesprint.GetRapport();

            Assert.Equal(rapport, getRapport);
        }

        [Fact]
        public void ReviewSprintGivesPlannedInStatus()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            string getstatus = reviewsprint.GetStatus();

            Assert.Equal(STATUS.PlannedIn.ToString(), getstatus);
        }

        [Fact]
        public void DeploymentSprintGivesPlannedInStatus()
        {
            Backlog backlog = new Backlog();
            ISprint deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");

            string getstatus = deploymentsprint.GetStatus();

            Assert.Equal(STATUS.PlannedIn.ToString(), getstatus);
        }

        [Fact]
        public void ReleaseSprintGivesPlannedInStatus()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");

            string getstatus = releasesprint.GetStatus();

            Assert.Equal(STATUS.PlannedIn.ToString(), getstatus);
        }

        [Fact]
        public void ReviewSprintUpdatingStatusGivesCorrectStatus()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");
            IStatus newstatus = new Finished();
            reviewsprint.UpdateStatus(newstatus);

            string getstatus = reviewsprint.GetStatus();

            Assert.Equal(STATUS.Finished.ToString(), getstatus);
        }

        [Fact]
        public void DeploymentSprintUpdatingStatusGivesCorrectStatus()
        {
            Backlog backlog = new Backlog();
            ISprint deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");
            IStatus newstatus = new Finished();
            deploymentsprint.UpdateStatus(newstatus);

            string getstatus = deploymentsprint.GetStatus();

            Assert.Equal(STATUS.Finished.ToString(), getstatus);
        }

        [Fact]
        public void ReleaseSprintUpdatingStatusGivesCorrectStatus()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");
            IStatus newstatus = new Finished();
            releasesprint.UpdateStatus(newstatus);

            string getstatus = releasesprint.GetStatus();

            Assert.Equal(STATUS.Finished.ToString(), getstatus);
        }

        [Fact]
        public void ReleaseSprintPipelineRunningUnfinishedGivesTrue()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");

            bool getpipelinestatus = releasesprint.RunPipeline();

            Assert.True(getpipelinestatus);
        }

        [Fact]
        public void ReleaseSprintPipelineRunningfinishedGivesfalse()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");
            releasesprint.UpdateStatus(new Finished());

            bool getpipelinestatus = releasesprint.RunPipeline();

            Assert.False(getpipelinestatus);
        }

        [Fact]
        public void DeploymentSprintPipelineRunningUnfinishedGivesTrue()
        {
            Backlog backlog = new Backlog();
            ISprint deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");

            bool getpipelinestatus = deploymentsprint.RunPipeline();

            Assert.True(getpipelinestatus);
        }

        [Fact]
        public void DeploymentSprintPipelineRunningfinishedGivesfalse()
        {
            Backlog backlog = new Backlog();
            ISprint deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");
            deploymentsprint.UpdateStatus(new Finished());

            bool getpipelinestatus = deploymentsprint.RunPipeline();

            Assert.False(getpipelinestatus);
        }

        [Fact]
        public void ReviewSprintPipelineRunningUnfinishedGivesfalse()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            bool getpipelinestatus = reviewsprint.RunPipeline();

            Assert.False(getpipelinestatus);
        }

        [Fact]
        public void ReviewSprintChangenameGivesCorrectName()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");
            string newname = "cheeseknabbels";

            reviewsprint.SetName(newname);

            Assert.Equal(newname, reviewsprint.GetName());
        }

        [Fact]
        public void DeploymentSprintChangenameGivesCorrectName()
        {
            Backlog backlog = new Backlog();
            ISprint Deploymentsprint = new DeploymentSprint(backlog, 2, "Testsprint");
            string newname = "cheeseknabbels";

            Deploymentsprint.SetName(newname);

            Assert.Equal(newname, Deploymentsprint.GetName());
        }

        [Fact]
        public void ReleaseSprintChangenameGivesCorrectName()
        {
            Backlog backlog = new Backlog();
            ISprint releasesprint = new ReleaseSprint(backlog, 2, "Testsprint");
            string newname = "cheeseknabbels";

            releasesprint.SetName(newname);

            Assert.Equal(newname, releasesprint.GetName());
        }

        [Fact]
        public void ReviewSprintChangenameOnFinishDoesntGivesnewName()
        {
            Backlog backlog = new Backlog();
            string oldname = "Testsprint";
            ISprint reviewsprint = new ReviewSprint(backlog, 2, oldname);
            string newname = "cheeseknabbels";

            reviewsprint.UpdateStatus(new Finished());
            reviewsprint.SetName(newname);

            Assert.Equal(oldname, reviewsprint.GetName());
        }

        [Fact]
        public void DeploymentSprintChangenameOnFinishDoesntGivesnewName()
        {
            Backlog backlog = new Backlog();
            string oldname = "Testsprint";
            ISprint Deploymentsprint = new DeploymentSprint(backlog, 2, oldname);
            string newname = "cheeseknabbels";

            Deploymentsprint.UpdateStatus(new Finished());
            Deploymentsprint.SetName(newname);

            Assert.Equal(oldname, Deploymentsprint.GetName());
        }

        [Fact]
        public void ReleaseSprintChangenameOnFinishDoesntGivesnewName()
        {
            Backlog backlog = new Backlog();
            string oldname = "Testsprint";
            ISprint releasesprint = new ReleaseSprint(backlog, 2, oldname);
            string newname = "cheeseknabbels";

            releasesprint.UpdateStatus(new Finished());
            releasesprint.SetName(newname);

            Assert.Equal(oldname, releasesprint.GetName());
        }

        [Fact]
        public void ReviewSprintAddRapportGivesCorrectReviewSprintStatus()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            ExportStrategy strat = new ExportToPng();
            Rapport rapport = new Rapport(strat, reviewsprint);
            reviewsprint.AddRapport(rapport);

            Assert.Equal(STATUS.Finished.ToString(), reviewsprint.GetStatus());
        }

        [Fact]
        public void ReviewSprintCheckSprintGivesCorrectReviewSprintStatus()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            ExportStrategy strat = new ExportToPng();
            Rapport rapport = new Rapport(strat, reviewsprint);
            reviewsprint.AddRapport(rapport);

            Assert.True(reviewsprint.CheckSprintStatus());
        }

        [Fact]
        public void DeploymentCheckSprintGivesCorrectTrueAfterExtendedTime()
        {
            Backlog backlog = new Backlog();
            DateTime day = DateTime.Now;
            int weeknumber = day.DayOfYear / 7;
            ISprint Deploymentsprint = new DeploymentSprint(backlog, weeknumber - 1, "Testsprint");

            Deploymentsprint.CheckSprintStatus();

            Assert.Equal(STATUS.Finished.ToString(), Deploymentsprint.GetStatus());
        }

        [Fact]
        public void ReleaseSprintCheckSprintGivesTrueAfterExtendedTime()
        {
            Backlog backlog = new Backlog();
            DateTime day = DateTime.Now;
            int weeknumber = day.DayOfYear / 7;
            ISprint releasesprint = new ReleaseSprint(backlog, weeknumber - 1, "Testsprint");

            releasesprint.CheckSprintStatus();

            Assert.Equal(STATUS.Finished.ToString(), releasesprint.GetStatus());
        }

        [Fact]
        public void ReviewSprintCheckSprintGivesFalseIfNotFinished()
        {
            Backlog backlog = new Backlog();
            ISprint reviewsprint = new ReviewSprint(backlog, 2, "Testsprint");

            Assert.False(reviewsprint.CheckSprintStatus());
        }

        [Fact]
        public void DeploymentCheckSprintGivesFalseBeforeExtendedTime()
        {
            Backlog backlog = new Backlog();
            DateTime day = DateTime.Now;
            int weeknumber = day.DayOfYear / 7;
            ISprint Deploymentsprint = new DeploymentSprint(backlog, weeknumber, "Testsprint");

            Assert.False(Deploymentsprint.CheckSprintStatus());
        }

        [Fact]
        public void ReleaseSprintCheckSprintGivesFalseBeforeExtendedTime()
        {
            Backlog backlog = new Backlog();
            DateTime day = DateTime.Now;
            int weeknumber = day.DayOfYear / 7;
            ISprint releasesprint = new ReleaseSprint(backlog, weeknumber, "Testsprint");

            Assert.False(releasesprint.CheckSprintStatus());
        }

    }
}
